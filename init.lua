conf = {
  redis = {
    host = "redis",
    port = 6379
  },
  jwt = {
    -- you really really really want to change this
    secret = "iminurappzsigningurtokenz"
  },
  app = {
    items_per_page = 10
  }
}
