local json   = require "cjson"
local utils  = require "ao_helpers"
local model  = require "data"
local auth   = require "auth"

local red, err = model.redis_connect()
if not red then
  return utils.internal_server_error("Misconfigured database: " .. err)
end

local method = ngx.req.get_method()
local uri = ngx.var.uri
local handler = ngx.var.handler
local campaign_id = ngx.var.campaign_id

-- Helpers ---------------------------------------------------------------------

local function validate_campaign_request(params)
  local required_params = { "if", "then", "type", "initiator", "deadline" }
  local known_types = {
    public = true,
    private = true,
    public_if_successful = true
  }

  if not params then
    return false
  end

  local request_valid = true
  for _, key in ipairs(required_params) do
    if not params[key] or #tostring(params[key]) == 0 then
      request_valid = false
    end
  end

  if not known_types[params.type] then
    request_valid = false
  end

  return request_valid
end

-- Handlers --------------------------------------------------------------------

-- POST /campaign
if method == "POST" and handler == 'campaign' and #campaign_id == 0 then
  local params = utils.read_json_body()

  local request_valid = validate_campaign_request(params)
  if not request_valid then
    return utils.bad_request("Invalid request parameters")
  end

  -- the user can only create campaigns where they are the initiator
  auth.assert_logged_in_user_id(params.initiator)

  local campaign_id, err = model.add_campaign(red, params)

  if not campaign_id then
    return utils.bad_request("Unable to add campaign: " .. err)
  end

  return ngx.say(json.encode({ id = campaign_id }))
end

-- GET /campaign
if method == "GET" and handler == 'campaign' and #campaign_id == 0 then
  local args = ngx.req.get_uri_args()
  local campaigns, err = model.get_active_campaigns(red, args['p'])

  if not campaigns then
    return utils.internal_server_error("Unable to fetch campaigns: " .. err)
  end

  return ngx.say(json.encode({ campaigns = campaigns }))
end

-- GET /campaign/{uuid}
if method == "GET" and handler == 'campaign' and #campaign_id > 0 then
  local campaign = model.get_campaign_by_id(red, campaign_id)

  if campaign == nil then
    return utils.not_found("Campaign not found")
  end

  local initiator, ierr = model.get_user_by_id(red, campaign.initiator)
  local participants_count, perr = model.get_campaign_participants_count(
    red, campaign_id
  )

  if ierr or perr then
    return utils.bad_request(
      "Error while fetching campaign: " .. (ierr or perr)
    )
  end

  campaign.initiator = initiator
  campaign.number_of_participants = participants_count
  return ngx.say(json.encode(campaign))
end

-- DELETE /campaign/{uuid}
if method == "DELETE" and handler == 'campaign' and #campaign_id > 0 then
  local campaign = model.get_campaign_by_id(red, campaign_id)

  if campaign == nil then
    return utils.not_found("Campaign not found")
  end

  -- only the initiator can delete the campaign
  auth.assert_logged_in_user_id(campaign.initiator)

  local success, err = model.delete_campaign(red, campaign_id)

  if err then
    return utils.bad_request(
      "Error while deleting campaign: " .. err
    )
  end

  return ngx.say()
end

-- POST /campaign/(.*)/participants
if method == "POST"
   and handler == 'campaign_participants'
   and #campaign_id > 0 then
     local params = utils.read_json_body()

     if params.user == nil or #params.user == 0 then
       return utils.bad_request("Invalid request parameters")
     end

     -- the user can only join campaigns for themselves
     auth.assert_logged_in_user_id(params.user)

     local ok, err = model.join_campaign(red, params.user, campaign_id)

     if err then
       return ngx.say(json.encode( { success = false, error = err }))
     end

     return ngx.say(json.encode({ success = true }))
end

-- GET /campaign/(.*)/participants
if method == "GET"
  and handler == 'campaign_participants'
  and #campaign_id > 0 then
    local participants, err = model.get_campaign_participants(red, campaign_id)

    if err == "Not found" then
      return utils.not_found("Campaign not found")
    elseif err ~= nil then
      return utils.internal_server_error("Error fetching campaign: " .. err)
    end

    ngx.log(ngx.ERR, json.encode(participants))

    return ngx.say(json.encode({ participants = participants }))
end

return utils.not_found("Page not found")
