local redis  = require "resty.redis"
local uuid   = require "uuid"
local scrypt = require "scrypt"
local jwt    = require "resty.jwt"

-- if OpenResty's "lua_code_cache" directive is set to 'off', we need to re-seed
-- the UUID library's RNG here as well (otherwise, the call within
-- the `init_worker.lua` file would do.)
local AO_DEBUG = ngx.var.ao_debug
if AO_DEBUG then
  uuid.seed()
end

local _M = {}

-- Redis -----------------------------------------------------------------------

function _M.redis_connect()
  local red = redis:new()
  red:set_timeout(1000)

  local ok, err = red:connect(conf.redis.host, conf.redis.port)
  if not ok then return ok, err end

  return red
end

function list_to_obj(list)
  if #list % 2 ~= 0 then
    error("List cardinality has to be even")
  end

  local obj = {}
  for i = 1, #list, 2 do
    obj[list[i]] = list[i+1]
  end

  return obj
end

-- Users -----------------------------------------------------------------------

-- The Redis keys relevant for users are:
--   - ao:user:{uuid} -- a per-user hash containing basic user data (at a
--     minimum, it contains the user ID)
--   - ao:users:by-email -- a hash mapping emails to user IDs
--   - ao:users:registered-emails -- a set containing all registered email
--     addresses, used to ensure uniqueness

function add_email_user(red, user)
  user.password = scrypt.crypt(user.password)

  local user_key = "ao:user:" .. user.id

  local ok, err = red:multi()

  local ans, err = red:sismember('ao:users:registered-emails', user.username)
  red:hmset(user_key, user)
  red:sadd('ao:users:registered-emails', user.username)

  local res, err = red:exec()

  if res[1] == 1 then
    red:del(user_key)
    return nil, "User already present"
  end

  red:hset('ao:users:by-email', user.username, user.id)
  return user.id, nil
end

function _M.get_user_by_id(red, id)
  local user = red:hgetall('ao:user:' .. id)
  if #user == 0 then
    return nil
  end

  local user_obj = list_to_obj(user)
  user_obj.password = nil

  return user_obj
end

function _M.add_user(red, user)
  user.id = uuid()

  if user.auth == 'email' then
    return add_email_user(red, user)
  end

  return nil, "Unknown auth mechanism"
end

function _M.authorize_user(red, params)
  if params.auth ~= 'email' then
    return nil, "Unknown auth mechanism"
  end

  local user_id, ierr = red:hget('ao:users:by-email', params.username)
  if type(user_id) ~= 'string' or #user_id ~= 36 or ierr then
    return nil, "Unknown user"
  end

  local user, uerr = red:hgetall('ao:user:' .. user_id)
  if type(user) ~= 'table' or uerr then return nil, "Unable to fetch user" end

  local user_obj = list_to_obj(user)
  local pwd_valid = scrypt.check(params.password, user_obj.password)

  if pwd_valid then
    return user_obj.id, nil
  end

  return nil, "Invalid password"
end

-- Campaigns -------------------------------------------------------------------

-- The Redis keys relevant for campaigns are:
--   - ao:campaign:{uuid} -- a per-campaign hash containing basic data about the
--     campaign
--   - ao:campaigns_by_user:{uuid} -- a per-user hash containing UUIDs of all
--     campaigns started by the user with the ID {uuid}
--   - ao:campaign_participants:{uuid} -- a set containing UUIDs of all users who
--     decided to join the campaign
--   - ao:campaigns_by_deadline -- a sorted set containing campaign UUIDs, with
--     the score being the timestamp of the deadline of the campaign

function _M.get_campaign_by_id(red, id)
  local campaign = red:hgetall('ao:campaign:' .. id)
  if #campaign == 0 then
    return nil
  end

  local campaign_obj = list_to_obj(campaign)
  return campaign_obj
end

function _M.add_campaign(red, campaign)
  campaign.id = uuid()
  local campaign_key = "ao:campaign:" .. campaign.id

  local initiator = _M.get_user_by_id(red, campaign.initiator)
  if not initiator then
    return nil, "Unknown user " .. campaign.initiator
  end

  local ok, err = red:multi()

  local fields = { "if", "then", "additional_info", "initiator",
                   "type", "deadline" }
  for _, key in ipairs(fields) do
    if campaign[key] then
      red:hmset(campaign_key, key, campaign[key])
    end
  end

  red:sadd('ao:campaigns_by_user:' .. campaign.initiator, campaign.id)
  red:zadd('ao:campaigns_by_deadline', campaign.deadline, campaign.id)

  local res, err = red:exec()

  if err then
    return nil, "Unable to save campaign: " .. err
  end

  return campaign.id, nil
end

function _M.delete_campaign(red, id)
  local campaign = _M.get_campaign_by_id(red, id)
  if campaign == nil then
    return false, "Unable to delete campaign: unknown campaign"
  end

  local ok, err = red:multi()

  red:zrem('ao:campaigns_by_user:' .. campaign.initiator, id)
  red:zrem('ao:campaigns_by_deadline', id)
  red:del('ao:campaign_participants:' .. id)
  red:del('ao:campaign:' .. id)

  local res, err = red:exec()

  if err then
    return false, "Unable to delete campaign: " .. err
  end

  return true, nil
end

function _M.join_campaign(red, user_id, campaign_id)
  local initiator = _M.get_user_by_id(red, user_id)
  if not initiator then
    return nil, "Unknown user " .. user_id
  end

  local campaign = _M.get_campaign_by_id(red, campaign_id)
  if not campaign then
    return nil, "Unknown campaign " .. campaign_id
  end

  return red:sadd('ao:campaign_participants:' .. campaign_id, user_id)
end

function _M.get_campaign_participants(red, campaign_id)
  local campaign = _M.get_campaign_by_id(red, campaign_id)
  if not campaign then return nil, "Not found" end

  local participants, err = red:smembers(
    "ao:campaign_participants:" .. campaign_id
  )

  if err then return nil, err end

  local users = {}
  for _, participant in ipairs(participants) do
    local user = _M.get_user_by_id(red, participant)
    table.insert(users, user)
  end

  return users, nil
end

function _M.get_campaign_participants_count(red, campaign_id)
  local campaign = _M.get_campaign_by_id(red, campaign_id)
  if not campaign then return nil, "Not found" end

  local participants, err = red:scard(
    "ao:campaign_participants:" .. campaign_id
  )

  if err then return nil, err end
  return participants, nil
end

function _M.get_active_campaigns(red, pg)
  local page = pg or 1
  local time = ngx.time()

  local offset = (page - 1) * conf.app.items_per_page
  local limit = conf.app.items_per_page

  local campaign_ids = red:zrevrangebyscore(
    "ao:campaigns_by_deadline", "+inf", time, 'limit', offset, limit
  )

  local campaigns = {}
  for _, id in ipairs(campaign_ids) do
    local campaign = _M.get_campaign_by_id(red, id)

    if campaign.type == "public" then
      campaign.id = id
      table.insert(campaigns, campaign)
    end
  end

  return campaigns
end

return _M
