local json   = require "cjson"
local utils  = require "ao_helpers"
local model  = require "data"
local auth   = require "auth"

local red, err = model.redis_connect()
if not red then
  return utils.internal_server_error("Misconfigured database: " .. err)
end

local method = ngx.req.get_method()
local handler = ngx.var.handler

-- POST /user
if method == "POST" and handler == 'user' then
  local params = utils.read_json_body()

  if params.username == nil or params.password == nil or
     #params.username == 0 or #params.password == 0 then
       return utils.bad_request("Invalid request parameters")
  end

  local user_id, err = model.add_user(red, params)

  if not user_id then
    return utils.bad_request("Unable to add user: " .. err)
  end

  return ngx.say(json.encode({ id = user_id }))
end

-- POST /user/auth
if method == "POST" and handler == 'user_auth' then
  local params = utils.read_json_body()

  if params.username == nil or params.password == nil or
     #params.username == 0 or #params.password == 0 then
       return utils.bad_request("Invalid request parameters")
  end

  local user_id, err = model.authorize_user(red, params)

  if not user_id then
    return utils.unauthorized("Invalid credentials: " .. err)
  end

  local token = auth.generate_token({ user_id = user_id })
  return ngx.say(json.encode({ token = token }))
end
