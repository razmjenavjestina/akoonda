local jwt   = require "resty.jwt"
local utils = require "ao_helpers"

local function verify_token(token)
  local token = jwt:verify(conf.jwt.secret, token)

  if token.valid and token.verified then
    return token.payload, nil
  end

  return nil, token.reason
end

local _M = {}

function _M.generate_token(user)
  return jwt:sign(
    conf.jwt.secret,
    {
      header = { typ="JWT", alg="HS256" },
      payload = { user_id = user.user_id }
    }
  )
end

function _M.assert_logged_in()
  local auth_header = ngx.req.get_headers()["Authorization"]

  if not auth_header or auth_header:sub(1, 6) ~= "Bearer" then
    utils.unauthorized("Not authorized: Invalid token")
    return ngx.exit(ngx.HTTP_UNAUTHORIZED)
  end

  local token_str = auth_header:sub(8)
  local token, err = verify_token(token_str)

  if not token then
    utils.unauthorized("Not authorized: Invalid token")
    return ngx.exit(ngx.HTTP_UNAUTHORIZED)
  end

  return token, err
end

function _M.assert_logged_in_user_id(user_id)
  local token, err = _M.assert_logged_in()

  if token.user_id ~= user_id then
    utils.unauthorized("Not authorized: Wrong user")
    return ngx.exit(ngx.HTTP_UNAUTHORIZED)
  end

  return token, err
end

return _M
