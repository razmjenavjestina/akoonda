FROM openresty/openresty:xenial

RUN \
    apt-get update \
    && apt-get -y install libscrypt0 \
    && cp /usr/lib/libscrypt.so.0 /usr/lib/libscrypt.so \
    && apt install -y --no-install-recommends dnsmasq \
    && sed -i s/#user=/user=root/ /etc/dnsmasq.conf \
    && /usr/local/openresty/luajit/bin/luarocks install lua-resty-jwt