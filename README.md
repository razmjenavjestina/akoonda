# Akoonda

Pledgebank-like software.

## Dependencies

  - libscrypt (`libscrypt0` in Debian / Ubuntu)
  - Redis (https://redis.io/)
  - OpenResty (http://openresty.org/en/)

## Running the app

    docker-compose up

The application is served at `http://localhost:8080/`.

To inspect the Redis instance with redis-cli:

    docker run -it --link akoonda_redis_1:redis --net akoonda_default --rm redis redis-cli -h redis -p 6379

## API

### Register a user

    POST /user { "username": "moj@email.com",
                 "password": "hunter2",
                 "auth": "email" }
    -> { "id": "some-uuid" }

### Authenticate a user

    POST /user/auth { "username": "moj@email.com",
                      "password": "hunter2",
                      "auth": "email" }
    -> { "token": "json web token" }

### Create a campaign

    POST /campaign { "if": "15000 ili više ljudi pridruži",
                     "then": "14.5. u 19:00 očistiti dvorište ispred zgrade",
                     "additional_info": "Zgrada u Preradovićevoj 18 ...",
                     "initiator": "some-uuid",
                     "type": "public",
                     "deadline": 1484561032 }
    -> { "id": "some-uuid" }

    type = public | private | public_if_successful

### Add a participant

    POST /campaign/{UUID}/participants { "user": "some-uuid" }
    -> { "success": true } | { "success": false, "error": "error info" }

### Get a list of people who joined a campaign

    GET /campaign/{UUID}/participants
    -> { "participants": [ { "id": "some-uuid" } ] }

### Get a list of active campaigns

Only fetches active (i.e. unexpired) *public* campaigns.

Displays `conf.app.items_per_page` items per page (see `init.lua`). Accepts an
optional GET parameter `p`, designating the page to display.

    GET /campaign
    -> { "campaigns": [
         { "id": "some-uuid",
           "if": "...",
           "then": "...",
           "additional_info": "...",
           "type": "public",
           "deadline": "..." }
       ] }

### Fetch a campaign

    GET /campaign/{UUID}
    -> { "id": "some-uuid",
         "if": "...",
         "then": "...",
         "additional_info": "...",
         "initiator": { "id": "some-uuid" },
         "type": "public",
         "number_of_participants": 666 }
