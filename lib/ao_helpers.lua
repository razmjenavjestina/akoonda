local json  = require "cjson"

local function http_err(s, m)
  ngx.status = s
  ngx.say(json.encode({ error = m }))
  return
end

local _M = {}

function _M.read_json_body()
  ngx.req.read_body()
  local body = ngx.req.get_body_data()

  if body == nil or #body == 0 then
    return _M.bad_request("Empty request")
  end

  return json.decode(body)
end

function _M.internal_server_error(error_message)
  return http_err(ngx.HTTP_INTERNAL_SERVER_ERROR, error_message)
end

function _M.bad_request(error_message)
  return http_err(ngx.HTTP_BAD_REQUEST, error_message)
end

function _M.not_found(error_message)
  return http_err(ngx.HTTP_NOT_FOUND, error_message)
end

function _M.unauthorized(error_message)
  return http_err(ngx.HTTP_UNAUTHORIZED, error_message)
end

return _M
