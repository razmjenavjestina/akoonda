* DONE User auth
  CLOSED: [2017-01-18 Wed 12:14]
  - JSON Web Tokens - https://github.com/SkyLothar/lua-resty-jwt
* DONE Campaign deadline
  CLOSED: [2017-01-19 Thu 11:51]
  - add a new sorted set that will serve as an index: ao:campaigns-by-deadline
  - score for the sorted set = timestamp of the deadline
  - query: https://redis.io/commands/zrangebyscore
* DONE List campaigns (GET /campaign)
  CLOSED: [2017-01-19 Thu 15:31]
* DONE Campaign list pagination
  CLOSED: [2017-02-23 Thu 20:36]
* DONE Delete campaign
  CLOSED: [2017-04-27 Thu 19:29]
* TODO Add Facebook accounts to already existing user accounts
* TODO Facebook-only login
* TODO Send email to participants when the campaign ends
  - https://github.com/catwell/tls-mailer
  - http://www.mailgun.com/ (10k free msgs / mo. + SMTP & HTTP API)

